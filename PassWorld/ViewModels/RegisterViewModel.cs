﻿using System;
using System.Diagnostics;
using Prism.Commands;
using Prism.Navigation;
using PassWorld.Services;
using PassWorld.Helpers;
using Prism.Services;
using PassWorld.Views;
using Xamarin.Forms;

namespace PassWorld.ViewModels
{
    public class RegisterViewModel : ViewModelBase
    {
        IPageDialogService _pageDialogService;

        public DelegateCommand CreateUserCommand { get; set; }

        PassWorldApiService _passWorldApiService;

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { SetProperty(ref _firstName, value); }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { SetProperty(ref _lastName, value); }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public RegisterViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService)
        {
            Title = "Create User";

            _passWorldApiService = new PassWorldApiService();
            _pageDialogService = pageDialogService;

            CreateUserCommand = new DelegateCommand(OnCreateUserTapped);
        }

        private async void OnCreateUserTapped()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnCreateUserTapped)}");
            object user = new
            {
                password = _password,
                firstName = _firstName,
                lastName = _lastName,
                email = _username
            };
            var res = await _passWorldApiService.PostRegisterAsync(user);
            if (res != null)
            {
                Settings.UserSetting = res.PassWorldUserResponseId;
                var navParameters = new NavigationParameters();
                navParameters.Add("user", res);
                await _navigationService.NavigateAsync($"/{nameof(MenuPage)}/{nameof(NavigationPage)}/{nameof(PasswordManagerPage)}", navParameters);            }
            else
                await _pageDialogService.DisplayAlertAsync("Authenticate Failed", "Wrong username or password", "Ok");
        }
    }
}
