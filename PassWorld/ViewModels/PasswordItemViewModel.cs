﻿using System;
using Prism.Navigation;
using PassWorld.Models;
using Prism.Commands;
using Plugin.Fingerprint;
using Prism.Services;
using Plugin.Clipboard;

namespace PassWorld.ViewModels
{
    public class PasswordItemViewModel : ViewModelBase
    {
        IPageDialogService _pageDialogService;

        public DelegateCommand ShowPasswordCommand { get; set; }
        public DelegateCommand CopyPasswordCommand { get; set; }

        private string _website;
        public string Website
        {
            get { return _website; }
            set { SetProperty(ref _website, value); }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _note;
        public string Note
        {
            get { return _note; }
            set { SetProperty(ref _note, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private bool _isPassword;
        public bool IsPassword
        {
            get { return _isPassword; }
            set { SetProperty(ref _isPassword, value); }
        }

        private bool _canShow;

        public PasswordItemViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService)
        {
            Title = "Password Info";
            IsPassword = true;
            _canShow = false;

            _pageDialogService = pageDialogService;

            ShowPasswordCommand = new DelegateCommand(OnShowPasswordTapped);
            CopyPasswordCommand = new DelegateCommand(OnCopyPasswordTapped);
        }

        private async void OnCopyPasswordTapped()
        {
            var result = await CrossFingerprint.Current.IsAvailableAsync(true);
            if (result)
            {
                var auth = await CrossFingerprint.Current.AuthenticateAsync("Authenticate");
                if (auth.Authenticated)
                    CrossClipboard.Current.SetText(_password);
                else
                    await _pageDialogService.DisplayAlertAsync("Authenticate Failed", "Fingerprint Authentication Failed", "Ok");
            }
            else
                CrossClipboard.Current.SetText(_password);
        }

        private async void OnShowPasswordTapped()
        {
            if (!_canShow)
            {
                _canShow = true;

                var result = await CrossFingerprint.Current.IsAvailableAsync(true);
                if (result)
                {
                    var auth = await CrossFingerprint.Current.AuthenticateAsync("Authenticate");
                    if (auth.Authenticated)
                        IsPassword = false;
                    else
                        await _pageDialogService.DisplayAlertAsync("Authenticate Failed", "Fingerprint Authentication Failed", "Ok");
                }
                else
                    IsPassword = false;
            }
            else
            {
                _canShow = false;
                IsPassword = true;
            }
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters != null && parameters.ContainsKey("PassInfo"))
            {
                var PassInfo = (PasswordItem)parameters["PassInfo"];
                Website = PassInfo.Website == null ? "N/A" : PassInfo.Website;
                Name = PassInfo.Name == null ? "N/A" : PassInfo.Name;
                Username = PassInfo.Username == null ? "N/A" : PassInfo.Username;
                Note = PassInfo.Note == null ? "N/A" : PassInfo.Note;
                Password = PassInfo.Password == null ? "N/A" : PassInfo.Password;
            }
        }

    }
}
