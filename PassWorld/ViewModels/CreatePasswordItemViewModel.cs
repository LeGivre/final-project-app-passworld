﻿using System;
using Prism.Commands;
using Prism.Navigation;
using PassWorld.Helpers;
using Prism.Services;
using PassWorld.Services;
using System.Diagnostics;

namespace PassWorld.ViewModels
{
    public class CreatePasswordItemViewModel : ViewModelBase
    {
        IPageDialogService _pageDialogService;

        public DelegateCommand GenerateCommand { get; set; }
        public DelegateCommand CreateCommand { get; set; }

        PassWorldApiService _passWorldApiService;

        private string _website;
        public string Website
        {
            get { return _website; }
            set { SetProperty(ref _website, value); }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _note;
        public string Note
        {
            get { return _note; }
            set { SetProperty(ref _note, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                SetProperty(ref _password, value);
            }
        }

        private string _sliderLength;
        public string SliderLength
        {
            get { return _sliderLength; }
            set { SetProperty(ref _sliderLength, value); }
        }

        private bool _lowercase;
        public bool Lowercase
        {
            get { return _lowercase; }
            set { SetProperty(ref _lowercase, value); }
        }


        private bool _uppercase;
        public bool Uppercase
        {
            get { return _uppercase; }
            set { SetProperty(ref _uppercase, value); }
        }

        private bool _numeric;
        public bool Numeric
        {
            get { return _numeric; }
            set { SetProperty(ref _numeric, value); }
        }

        private bool _special;
        public bool Special
        {
            get { return _special; }
            set { SetProperty(ref _special, value); }
        }

        public CreatePasswordItemViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
           : base(navigationService)
        {
            Title = "Create Password";
            _sliderLength = "8";

            Uppercase = true;
            Lowercase = true;

            _passWorldApiService = new PassWorldApiService();
            _pageDialogService = pageDialogService;

            GenerateCommand = new DelegateCommand(OnGenerateTapped);
            CreateCommand = new DelegateCommand(OnCreateTapped);
        }

        private async void OnCreateTapped()
        {
            if (string.IsNullOrEmpty(Username) && (string.IsNullOrEmpty(Website) || string.IsNullOrEmpty(Name)))
                await _pageDialogService.DisplayAlertAsync("Creation Error", "You need at least a Username, a Website or a name to create a password!", "Ok");
            else
            {
                object newPass = new
                {
                    Website = _website == null ? null : _website,
                    Username = _username == null ? null : _username,
                    Password = _password == null ? null : _password,
                    Name = _name == null ? null : _name,
                    Note = _note == null ? null : _note,
                    UserId = Settings.UserSetting
                };
                try
                {
                    var response = await _passWorldApiService.PostPasswordAsync(newPass);
                    if (response == null)
                        await _pageDialogService.DisplayAlertAsync("Creation Error", "An Error occured. \nPlease try again.", "Ok");
                    else
                    {
                        var navParams = new NavigationParameters();
                        navParams.Add("NewPass", response);
                        await _navigationService.GoBackAsync(navParams);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnCreateTapped)}: EXCEPTION!!! {ex}");
                }
            }
        }

        private async void OnGenerateTapped()
        {
            if (_lowercase || _uppercase || _numeric || _special)
            {
                int length = Convert.ToInt32(_sliderLength);
                string GenPass = PasswordGenerator.GeneratePassword(_lowercase, _uppercase, _numeric, _special, false, length);

                while (!PasswordGenerator.PasswordIsValid(_lowercase, _uppercase, _numeric, _special, false, GenPass))
                {
                    GenPass = PasswordGenerator.GeneratePassword(_lowercase, _uppercase, _numeric, _special, false, length);
                }

                Password = GenPass;
            }
            else
                await _pageDialogService.DisplayAlertAsync("Generation Error", "You need at least one parameter to generate a password!", "Ok");

        }
    }
}
