﻿using System;
using System.Collections.ObjectModel;
using Prism.Navigation;
using PassWorld.Models;
using Prism.Commands;
using Xamarin.Forms;
using PassWorld.Views;

namespace PassWorld.ViewModels
{
    public class MenuPageViewModel : ViewModelBase
    {
        public ObservableCollection<MyMenuItem> MenuItems { get; set; }

        private String _menuTitle;
        public String MenuTitle
        {
            get => _menuTitle;
            set => SetProperty(ref _menuTitle, value);
        }

        private MyMenuItem _selectedMenuItem;
        public MyMenuItem SelectedMenuItem
        {
            get => _selectedMenuItem;
            set => SetProperty(ref _selectedMenuItem, value);
        }

        private static PassWorldUserResponse _user;
        public PassWorldUserResponse User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        public DelegateCommand NavigateCommand { get; private set; }

        public MenuPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            MenuItems = new ObservableCollection<MyMenuItem>();

            MenuItems.Add(new MyMenuItem()
            {
                //Icon = "ic_viewa",
                PageName = nameof(PasswordManagerPage),
                Title = "Password Manager"
            });

            MenuItems.Add(new MyMenuItem()
            {
                //Icon = "ic_viewb",
                PageName = nameof(PasswordGenetorPage),
                Title = "Password generator"
            });

            MenuItems.Add(new MyMenuItem()
            {
                //Icon = "ic_viewb",
                //PageName = nameof(PasswordGenetorPage),
                Title = "Profile"
            });

            NavigateCommand = new DelegateCommand(Navigate);
        }

        async void Navigate()
        {
            await _navigationService.NavigateAsync($"{nameof(NavigationPage)}/{SelectedMenuItem.PageName}");
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            if (_user == null)
            {
                _user = (PassWorldUserResponse)parameters["user"];
                MenuTitle = $"Account: {_user.FirstName} {_user.LastName}";

            }
        }

    }
}
