﻿using System;
using Prism.Navigation;
using PassWorld.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Diagnostics;
using Prism.Commands;
using PassWorld.Services;
using PassWorld.Views;

namespace PassWorld.ViewModels
{
    public class PasswordManagerViewModel : ViewModelBase
    {
        public DelegateCommand<PasswordItem> DeleteCommand { get; set; }
        public DelegateCommand<PasswordItem> InfoCommand { get; set; }
        public DelegateCommand PullToRefreshCommand { get; set; }
        public DelegateCommand CreateCommand { get; set; }

        PassWorldApiService _passWorldApiService;

        private ObservableCollection<PasswordItem> _grouped;
        public ObservableCollection<PasswordItem> Grouped
        {
            get { return _grouped; }
            set { SetProperty(ref _grouped, value); }
        }

        private PasswordItem _selectedPassword;
        public PasswordItem SelectedPassword
        {
            get { return _selectedPassword; }
            set { SetProperty(ref _selectedPassword, value); }
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetProperty(ref _isRefreshing, value); }
        }

        public PasswordManagerViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Password Manager";
            _passWorldApiService = new PassWorldApiService();
            PullToRefreshCommand = new DelegateCommand(OnPullToRefresh);
            DeleteCommand = new DelegateCommand<PasswordItem>(OnDeleteTapped);
            InfoCommand = new DelegateCommand<PasswordItem>(OnInfoTapped);
            CreateCommand = new DelegateCommand(OnCreateTapped);

            IsRefreshing = true;
            RefreshGroupedListAsync();
        }

        private async void OnInfoTapped(PasswordItem passwordToInfo)
        {
            var navParams = new NavigationParameters();
            navParams.Add("PassInfo", passwordToInfo);
            await _navigationService.NavigateAsync(nameof(PasswordItemPage), navParams);
        }

        private async void OnCreateTapped()
        {
            await _navigationService.NavigateAsync(nameof(CreatePasswordItemPage));
        }

        private async void OnDeleteTapped(PasswordItem passwordToDelete)
        {
            var response = await _passWorldApiService.DeletePasswordAsync(passwordToDelete.PasswordItemId);
            RefreshGroupedListAsync();
        }

        private async void OnPullToRefresh()
        {
            await RefreshGroupedListAsync();
        }

        private async Task RefreshGroupedListAsync()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshGroupedListAsync)}");
            try
            {
                Grouped = await _passWorldApiService.GetPasswordListAsync();
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshGroupedListAsync)}");
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RefreshGroupedListAsync)}: EXCEPTION!!! {ex}");
            }
            finally
            {
                IsRefreshing = false;
            }
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters != null && parameters.ContainsKey("NewPass"))
            {
                var NewPass = (PasswordItem)parameters["NewPass"];
                Grouped.Add(NewPass);
            }
        }
    }
}
