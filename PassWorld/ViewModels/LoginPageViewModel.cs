﻿using System.Diagnostics;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using PassWorld.Views;
using Xamarin.Forms;
using Plugin.Fingerprint;
using Prism.Services;
using PassWorld.Helpers;
using PassWorld.Services;
using PassWorld.Models;
using System;

namespace PassWorld.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {

        IPageDialogService _pageDialogService;

        public DelegateCommand ShowActionSheetCommand { get; set; }
        public DelegateCommand NormalLoginCommand { get; set; }
        public DelegateCommand FingerprintLoginCommand { get; set; }
        public DelegateCommand RegisterCommand { get; set; }

        PassWorldApiService _passWorldApiService;

        private string user_id;

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public LoginPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService)
        {
            Title = "Passworld";

            user_id = Settings.UserSetting;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(LoginPageViewModel)} : {user_id}");
            _passWorldApiService = new PassWorldApiService();
            _pageDialogService = pageDialogService;

            NormalLoginCommand = new DelegateCommand(OnNormalLoginTapped);
            RegisterCommand = new DelegateCommand(OnRegisterTapped);
            FingerprintLoginCommand = new DelegateCommand(OnFingerprintLoginTapped);
        }

        private async void OnRegisterTapped()
        {
            await _navigationService.NavigateAsync(nameof(RegisterPage));
        }

        private async void OnNormalLoginTapped()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNormalLoginTapped)}");
            var res = await _passWorldApiService.PostSigninAsync(_username, _password);
            if (res != null)
            {
                Settings.UserSetting = res.PassWorldUserResponseId;
                NavigateToHomePage(res);
            }
            else
                await _pageDialogService.DisplayAlertAsync("Authenticate Failed", "Wrong username or password", "Ok");

        }

        private async void OnFingerprintLoginTapped()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnFingerprintLoginTapped)}");
            if (string.IsNullOrEmpty(user_id))
                await _pageDialogService.DisplayAlertAsync("Error", "You have to login with your email and password first!", "Ok");
            else
            {
                var result = await CrossFingerprint.Current.IsAvailableAsync(true);
                if (result)
                {
                    var auth = await CrossFingerprint.Current.AuthenticateAsync("Authenticate");
                    if (auth.Authenticated)
                    {
                        var res = await _passWorldApiService.GetUserAsync(user_id);
                        if (res != null)
                            NavigateToHomePage(res);
                        else
                            await _pageDialogService.DisplayAlertAsync("Authenticate Failed", "Fingerprint Authentication Failed. Please login with your username and pasword", "Ok");
                    }
                    else
                    {
                        await _pageDialogService.DisplayAlertAsync("Authenticate Failed", "Fingerprint Authentication Failed", "Ok");
                    }
                }
            }
        }

        public async void NavigateToHomePage(PassWorldUserResponse user)
        {
            var navParameters = new NavigationParameters();
            navParameters.Add("user", user);
            await _navigationService.NavigateAsync($"/{nameof(MenuPage)}/{nameof(NavigationPage)}/{nameof(PasswordManagerPage)}", navParameters);
        }
    }
}