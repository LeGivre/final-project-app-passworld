﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PassWorld.Models;
//
//    var passwordItem = PasswordItem.FromJson(jsonString);

namespace PassWorld.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class PasswordItem
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("Website")]
        public string Website { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Note")]
        public string Note { get; set; }

        [JsonProperty("id")]
        public string PasswordItemId { get; set; }

        [JsonProperty("__v")]
        public long V { get; set; }
    }

    public partial class PasswordItem
    {
        public static PasswordItem FromJson(string json) => JsonConvert.DeserializeObject<PasswordItem>(json, PassWorld.Models.Converter.Settings);
    }
}
