﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PassWorld.Models;
//
//    var passWorldUserResponse = PassWorldUserResponse.FromJson(jsonString);

namespace PassWorld.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class PassWorldUserResponse
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("id")]
        public string PassWorldUserResponseId { get; set; }

        [JsonProperty("hashPassword")]
        public string HashPassword { get; set; }

        [JsonProperty("__v")]
        public long V { get; set; }
    }

    public partial class PassWorldUserResponse
    {
        public static PassWorldUserResponse FromJson(string json) => JsonConvert.DeserializeObject<PassWorldUserResponse>(json, PassWorld.Models.Converter.Settings);
    }
}
