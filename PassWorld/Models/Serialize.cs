﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PassWorld.Models;
//
//    var passWorldPasswordResponse = PassWorldPasswordResponse.FromJson(jsonString);

namespace PassWorld.Models
{
    using Newtonsoft.Json;

    public static class Serialize
    {
        public static string ToJson(this PassWorldPasswordResponse[] self) => JsonConvert.SerializeObject(self, PassWorld.Models.Converter.Settings);
    }
}
