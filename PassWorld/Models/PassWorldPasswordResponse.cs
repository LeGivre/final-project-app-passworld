﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PassWorld.Models;
//
//    var passWorldPasswordResponse = PassWorldPasswordResponse.FromJson(jsonString);

namespace PassWorld.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class PassWorldPasswordResponse
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("Website")]
        public string Website { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Note")]
        public string Note { get; set; }

        [JsonProperty("id")]
        public string PassWorldPasswordResponseId { get; set; }

        [JsonProperty("__v")]
        public long V { get; set; }
    }

    public partial class PassWorldPasswordResponse
    {
        public static PassWorldPasswordResponse[] FromJson(string json) => JsonConvert.DeserializeObject<PassWorldPasswordResponse[]>(json, PassWorld.Models.Converter.Settings);
    }
}
