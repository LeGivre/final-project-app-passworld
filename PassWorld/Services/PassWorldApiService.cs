﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using PassWorld.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Text;
using PassWorld.Helpers;

namespace PassWorld.Services
{
    public class PassWorldApiService
    {
        private string _baseUrl = "http://127.0.0.1:3001";

        private HttpClient _httpClient;

        ObservableCollection<PasswordItem> grouped = null;

        public PassWorldApiService()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.Timeout = TimeSpan.FromSeconds(5);
        }

        public bool DoIHaveInternet()
        {
            // This method is already fully implemented... No todos in here!
            if (!CrossConnectivity.IsSupported)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  Not supported... Returning true no matter what.");
                return true;
            }

            bool hasNet = CrossConnectivity.Current.IsConnected;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DoIHaveInternet)}:  returning {hasNet}");
            return hasNet;
        }

        //////////////////////
        //USER ITEM API CALL//
        //////////////////////

        public async Task<PassWorldUserResponse> PostRegisterAsync(object user)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostRegisterAsync)}:  Returning null... We do not have internet.");
                return null;
            }

            var jsonObj = JsonConvert.SerializeObject(user);
            StringContent content = new StringContent(jsonObj.ToString(), Encoding.UTF8, "application/json");

            PassWorldUserResponse response = null;

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri($"{_baseUrl}/auth/register");
            request.Content = content;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostRegisterAsync)}: Sending request:{request.RequestUri.ToString()} & content : {content}");
            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: Request failed. StatusCode={httpResponseMessage.StatusCode}");
                        return null;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostRegisterAsync)}: json response:\n{responseAsString}");
                    PassWorldUserResponse resultsDeserialized = JsonConvert.DeserializeObject<PassWorldUserResponse>(responseAsString);
                    response = resultsDeserialized;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: EXCEPTION!!\n{ex}");
            }

            return response;
        }

        public async Task<PassWorldUserResponse> PostSigninAsync(String username, String password)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetUserAsync)}:  Returning null... We do not have internet.");
                return null;
            }

            object userInfos = new { email = username, password = password };
            var jsonObj = JsonConvert.SerializeObject(userInfos);
            StringContent content = new StringContent(jsonObj.ToString(), Encoding.UTF8, "application/json");

            PassWorldUserResponse response = null;


            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri($"{_baseUrl}/auth/sign_in/");
            request.Content = content;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: Sending request:{request.RequestUri.ToString()} & content : {content}");
            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: Request failed. StatusCode={httpResponseMessage.StatusCode}");
                        return null;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: json response:\n{responseAsString}");
                    PassWorldUserResponse resultsDeserialized = JsonConvert.DeserializeObject<PassWorldUserResponse>(responseAsString);
                    response = resultsDeserialized;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: EXCEPTION!!\n{ex}");
            }

            return response;
        }

        public async Task<PassWorldUserResponse> GetUserAsync(String id)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetUserAsync)}:  Returning null... We do not have internet.");
                return null;
            }

            PassWorldUserResponse response = null;

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri($"{_baseUrl}/user?id={id}");
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetUserAsync)}: Sending request:{request.RequestUri.ToString()}");

            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetUserAsync)}: Request failed. StatusCode={httpResponseMessage.StatusCode}");
                        return null;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetUserAsync)}: json response:\n{responseAsString}");
                    PassWorldUserResponse resultsDeserialized = JsonConvert.DeserializeObject<PassWorldUserResponse>(responseAsString);
                    response = resultsDeserialized;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetUserAsync)}: EXCEPTION!!\n{ex}");
            }

            return response;
        }

        //////////////////////////
        //PASSWORD ITEM API CALL//
        //////////////////////////

        public async Task<ObservableCollection<PasswordItem>> GetPasswordListAsync()
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetPasswordListAsync)}:  Returning null... We do not have internet.");
                return null;
            }

            ObservableCollection<PasswordItem> response = null;

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = new Uri($"{_baseUrl}/password/list?id={Settings.UserSetting}");
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetPasswordListAsync)}: Sending request:{request.RequestUri.ToString()}");

            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetPasswordListAsync)}: Request failed. StatusCode={httpResponseMessage.StatusCode}");
                        return null;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetPasswordListAsync)}: json response:\n{responseAsString}");
                    ObservableCollection<PasswordItem> resultsDeserialized = JsonConvert.DeserializeObject<ObservableCollection<PasswordItem>>(responseAsString);
                    response = resultsDeserialized;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetUserAsync)}: EXCEPTION!!\n{ex}");
            }

            return response;
        }

        public async Task<PasswordItem> PostPasswordAsync(object newPass)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostPasswordAsync)}:  Returning null... We do not have internet.");
                return null;
            }

            var jsonObj = JsonConvert.SerializeObject(newPass);
            StringContent content = new StringContent(jsonObj.ToString(), Encoding.UTF8, "application/json");

            PasswordItem response = null;


            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Post;
            request.RequestUri = new Uri($"{_baseUrl}/password?id={Settings.UserSetting}");
            request.Content = content;
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostPasswordAsync)}: Sending request:{request.RequestUri.ToString()} & content : {content}");
            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostPasswordAsync)}: Request failed. StatusCode={httpResponseMessage.StatusCode}");
                        return null;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostPasswordAsync)}: json response:\n{responseAsString}");
                    PasswordItem resultsDeserialized = JsonConvert.DeserializeObject<PasswordItem>(responseAsString);
                    response = resultsDeserialized;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: EXCEPTION!!\n{ex}");
            }

            return response;
        }

        public async Task<bool> DeletePasswordAsync(string id)
        {
            if (DoIHaveInternet() == false)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DeletePasswordAsync)}:  Returning null... We do not have internet.");
                return false;
            }

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Delete;
            request.RequestUri = new Uri($"{_baseUrl}/password/delete/{id}");
            try
            {
                using (HttpResponseMessage httpResponseMessage = await _httpClient.SendAsync(request))
                {
                    if (httpResponseMessage.IsSuccessStatusCode == false)
                    {
                        Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DeletePasswordAsync)}: Request failed. StatusCode={httpResponseMessage.StatusCode}");
                        return false;
                    }
                    string responseAsString = await httpResponseMessage.Content.ReadAsStringAsync();
                    Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostPasswordAsync)}: json response:\n{responseAsString}");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(PostSigninAsync)}: EXCEPTION!!\n{ex}");
            }
            return true;
        }
    }
}
