'use strict'

var mongoose = require('mongoose')
var bcrypt = require('bcrypt')

var passwordSchema = new mongoose.Schema({
  id: {
    type: String,
    unique: false,
    required: false,
    trim: true
  },
  Website: {
    type: String,
    unique: false,
    required: false,
    trim: true
  },
  Username: {
    type: String,
    unique: false,
    required: false,
    trim: true
  },
  Password: {
    type: String,
    unique: false,
    required: false,
    trim: true
  },
  Name: {
    type: String,
    unique: false,
    required: false,
    trim: true
  },
  Note: {
    type: String,
    unique: false,
    required: false,
    trim: true
  }
})

const Password = mongoose.model('Password', passwordSchema)
module.exports = Password