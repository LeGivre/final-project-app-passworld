'use strict'

var mongoose = require('mongoose')
var bcrypt = require('bcrypt')

var userSchema = new mongoose.Schema({
  id: {
    type: String,
    unique: false,
    required: true,
    trim: true
  },
  hashPassword: {
    type: String,
    unique: false,
    required: true,
    trim: true
  },
  firstName: {
    type: String,
    unique: false,
    required: true,
    trim: true
  },
  lastName: {
    type: String,
    unique: false,
    required: true,
    trim: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  passwordsIds: {
    type: Array,
    unique: false,
    required: false,
    trim: true
  }
})

userSchema.methods.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.hashPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User