'use strict'

var express = require('express')
const app = express()
const port = process.env.PORT || 3001
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const morgan = require('morgan')

// connect to database
mongoose.Promise = global.Promise
mongoose.connect(process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/passwold')
mongoose.connection.on('error', (err) => {
    console.error(err)
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.')
    process.exit()
})

// configuration of the server
app.set('port', process.env.PORT || port)
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())
app.use(morgan('dev'))

app.use('/', require('./Routes/user'))
app.use('/', require('./Routes/password'))

// basic 404 handler
app.use((req, res) => {
    res.status(404).send({ url: req.originalUrl + ' Not Found' })
})

// basic error handler
app.use((err, req, res, next) => {
    console.error()
    res.status(err.code || 500).send(err.message || 'Something broke!')
})

// start the server
if (module === require.main) {
    const server = app.listen(app.get('port'), () => {
        const port = server.address().port
        console.log(`App listening on port ${port}`)
    })
}

module.exports = app