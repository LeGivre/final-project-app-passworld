'use strict'

const express = require('express')
const Password = require('../models/password')
const User = require('../models/user')
const uuid = require('uuid/v4')

const router = express.Router()

router.post('/password', function (req, res, next) {
    User.findOne({
        id: req.query.id
    }, function (err, user) {
        if (err) {
            return next(err)
        }
        if (!user) {
            return next({ code: 404, message: "User not found!" })
        } else {
            var newPass = new Password(req.body)
            newPass.id = uuid();
            user.passwordsIds.push(newPass.id)
            newPass.save(function (err, resp) {
                if (err) {
                    return res.json({ err })
                } else {
                    user.save((err, result) => {
                        if (err) {
                            return res.json({ err })
                        } else
                            return res.status(201).json(resp)
                    })
                }
            })
        }
    })

})

router.get('/password/list', function (req, se, next) {
    User.findOne({
        id: req.query.id
    }, function (err, user) {
        if (err) {
            return next(err)
        }
        if (!user) {
            return next({ code: 404, message: "User not found!" })
        } else {
            Password.find({
                'id': { $in: user.passwordsIds}
            }, function (err, passList) {
                if (err) {
                    return next(err)
                }
                se.status(200).json(passList)
            })
        }
    })
})

router.delete('/password/delete/:id', function (req, se) {
    Password.findOne(
        { id: req.params.id })
        .exec(function (err, res) {
            if (err) {
                console.log(err)
                return se.status(500).send(err)
            }
            if (!res) {
                se.sendStatus(204)
            } else {
                res.remove()
                se.sendStatus(200)
            }
        })
})

module.exports = router