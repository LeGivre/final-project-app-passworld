'use strict'

const express = require('express')
const bcrypt = require('bcrypt')
const User = require('../models/user')
const uuid = require('uuid/v4')

const router = express.Router()

router.post('/auth/register', function (req, res, next) {
    User.findOne({
        email: req.body.email
    }, function (err, guser) {
        if (err)
            return next(err)
        else if (!guser) {
            var newUser = new User(req.body)
            newUser.id = uuid();
            newUser.hashPassword = bcrypt.hashSync(req.body.password, 10)
            newUser.save(function (err, user) {
                if (err) {
                    return next(err)
                } else {
                    return res.status(201).json(newUser)
                }
            })
        }
        else {
            return next({ code: 409, message: 'Username already exist' })
        }
    })
})

router.post('/auth/sign_in', function (req, res, next) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) throw err
        if (!user) {
            return next({ code: 401, message: 'Authentication failed. User not found.' })
        } else if (user) {
            if (!user.comparePassword(req.body.password)) {
                return next({ code: 401, message: "Authentication failed. Wrong password." })
            } else {
                return res.status(200).json(user)
            }
        }
    })
})

router.get('/user', function (req, se, next) {
    User.findOne({
        id: req.query.id
    }, function (err, user) {
        if (err) {
            return next(err)
        }
        if (!user) {
            return next({ code: 404, message: "User not found!" })
        } else {
            se.status(200).json(user)
        }
    })
})

module.exports = router